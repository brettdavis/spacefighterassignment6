
#include "BossShip.h"

BossShip::BossShip()
{
	SetSpeed(50);
	SetMaxHitPoints(30);
	SetCollisionRadius(110);
}

BossShip::~BossShip()
{

}

void BossShip::Update(const GameTime *pGameTime)
{
	if (IsActive())
	{
		float y = 0;
		if (GetPosition().Y < 200)
		{
			y = GetSpeed() * pGameTime->GetTimeElapsed();
		}

		float x = sin(pGameTime->GetTotalTime() * Math::PI + GetIndex());
		x *= GetSpeed() * pGameTime->GetTimeElapsed() * 1.4f;
		TranslatePosition(x, y);

		if (!IsOnScreen()) Deactivate();
	}

	EnemyShip::Update(pGameTime);
}

void BossShip::Draw(SpriteBatch *pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(m_pTexture, GetPosition(), Color::White, m_pTexture->GetCenter(), Vector2::ONE, Math::PI, 1);
	}
}
