#include "Level02.h"
#include "BossShip.h"
#include "BioEnemyShip.h"

Level02::Level02()
{

}

Level02::~Level02()
{

}


void Level02::LoadContent(ResourceManager *pResourceManager)
{
	// Setup enemy ships
	Texture *pTexture = pResourceManager->Load<Texture>("Textures\\BossShip.png");
	Texture *pTexture2 = pResourceManager->Load<Texture>("Textures\\BioEnemyShip.png");

	const int COUNTBOSS = 1;

	double xPositionsBoss[COUNTBOSS] =
	{
		0.5
	};

	double delaysBoss[COUNTBOSS] =
	{
		0.5
	};

	float delay = 0.5; // start delay
	Vector2 positionBoss;

	

	for (int i = 0; i < COUNTBOSS; i++)
	{
		delay += delaysBoss[i];
		positionBoss.Set(xPositionsBoss[i] * Game::GetScreenWidth(), -pTexture2->GetCenter().Y - 95);

		BossShip *pEnemy = new BossShip();
		pEnemy->SetTexture(pTexture);
		pEnemy->SetCurrentLevel(this);
		pEnemy->Initialize(positionBoss, (float)delay);
		AddGameObject(pEnemy);
	}

	const int COUNTBIO = 27;

	double xPositionsBio[COUNTBIO] =
	{
		0.5, 0.2, 0.3,
		0.75, 0.8, 0.7,
		0.3, 0.25, 0.35, 0.2, 0.4,
		0.7, 0.75, 0.65, 0.8, 0.6,
		0.5, 0.4, 0.6, 0.45, 0.55,
		0.55, 0.25, 0.35,
		0.7, 0.85, 0.75
	};

	double delaysBio[COUNTBIO] =
	{
		0.1, 0.35, 0.35,
		1.0, 0.35, 0.35,
		1.25, 0.25, 0.25, 0.25, 0.25,
		1.25, 0.25, 0.25, 0.25, 0.25,
		3.5, 0.3, 0.3, 0.3, 0.3,
		0.25, 1.25, 0.25, 0.25, 0.25, 0.25
	};

	Vector2 positionBio;

	for (int i = 0; i < COUNTBIO; i++)
	{
		delay += delaysBio[i];
		positionBio.Set(xPositionsBio[i] * Game::GetScreenWidth(), -pTexture2->GetCenter().Y);

		BioEnemyShip *pEnemy2 = new BioEnemyShip();
		pEnemy2->SetTexture(pTexture2);
		pEnemy2->SetCurrentLevel(this);
		pEnemy2->Initialize(positionBio, (float)delay);
		AddGameObject(pEnemy2);
	}
	Level::LoadContent(pResourceManager);
}
